const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

app.get('/fast', function (req, res) {

    res.sendFile(__dirname + '/very-important-file.js', 'utf8', function (err, data) {
        console.log(">>> files was send");
    });
});

app.get('/slow', function (req, res) {

    setTimeout(() => {
        console.log(">>> wait 5 seconds");

        res.sendFile(__dirname + '/very-important-file.js', 'utf8', function (err, data) {
            console.log(">>> files was send");
        });
    }, 3000);
});

app.listen(9001, () => {
    
    console.log('>>> server is running');
}).on('error', function(message){ 

    console.log('>>> server error', message);
});