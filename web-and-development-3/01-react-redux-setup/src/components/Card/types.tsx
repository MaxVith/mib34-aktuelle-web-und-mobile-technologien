import * as CONSTANTS from './constants';

interface ICardFetchRequestAction {
    type: typeof CONSTANTS.FETCH_REQUEST
}
  
interface ICardFetchSuccessAction {
    type: typeof CONSTANTS.FETCH_SUCCESS,
    payload: {}
}

interface ICardFetchErrorAction {
    type: typeof CONSTANTS.FETCH_ERROR,
    error: string
}

export type CardActionTypes = ICardFetchRequestAction | ICardFetchSuccessAction | ICardFetchErrorAction;

export interface ICardStateProps {
    pending: boolean,
    payload?: {},
    errors?: string
}