import React from 'react';

import Teaser from './component';

const TeaserContainer = () => {

    return(
        <Teaser />
    );
};

export default TeaserContainer;