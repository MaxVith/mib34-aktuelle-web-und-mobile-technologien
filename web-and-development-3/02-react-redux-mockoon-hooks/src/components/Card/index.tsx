import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { ICardStateProps } from './types';
import { getCards } from './actions';
import { InitialState } from '../../store/types';

import Card from './component';
import Loader from '../../shared/Loader';

const CardContainer = () => {

    const { pending, payload } = useSelector<InitialState, ICardStateProps>((state: InitialState) => state.card);
    const dispatch = useDispatch();
    
    const [content, setContent] = useState<any>(<Loader />);

    if(pending){
        dispatch(getCards());
    }

    useEffect(() => {

        if(payload && Object.keys(payload).length > 0){

            setContent(
                payload.map((data, index) => {
                    return <Card key={ index } { ...data }/>;
                })
            );
        }
    }, [payload]);

    return(
        <div className="row mb-2">
            { content }
        </div>
    );
};

export default CardContainer;