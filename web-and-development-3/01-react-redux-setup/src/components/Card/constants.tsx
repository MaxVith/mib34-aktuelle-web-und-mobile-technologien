export const FETCH_REQUEST = "@@card/FETCH_REQUEST";
export const FETCH_SUCCESS = "@@card/FETCH_SUCCESS";
export const FETCH_ERROR = "@@card/FETCH_ERROR";