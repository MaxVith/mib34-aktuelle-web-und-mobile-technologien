# React

## Installation
- [Create React App](https://create-react-app.dev/)

### Creation a app without TypesScript
```javascript
$ npm init react-app my-app
```

### Creating a TypeScript app
```javascript
$ npm create-react-app my-app --template typescript
```

### SASS
Aktuell gibt es einen Bug (Error: Node Sass version 5.0.0 is incompatible with ^4.0.0.) mit der neusten Version von SASS + React. Als Workaround verwenden wir nicht node-sass sondern sass, wir weichen also von der Dokumentation ab (https://create-react-app.dev/docs/adding-a-sass-stylesheet). 

```javascript
$ npm install sass
```

### Bootstrap
- [React Bootstrap](https://react-bootstrap.github.io/)
- [Vanilla Bootstrap](https://getbootstrap.com/docs/4.5/getting-started/download/#npm)

```javascript
$ npm install bootstrap
```

### Router
- [Quick Start](https://reactrouter.com/web/guides/quick-start)
- [Types for React Router DOM](https://www.npmjs.com/package/@types/react-router-dom)

```javascript
$ npm install react-router-dom
$ npm install --save @types/react-router-dom
```

### Redux
- [Redux](https://redux.js.org/tutorials/fundamentals/part-5-ui-react)
- [Getting Started with Redux](https://redux.js.org/introduction/getting-started)

```javascript
$ npm install redux
$ npm install @types/react-redux
```

#### Redux Thunk
With a plain basic Redux store, you can only do simple synchronous updates by dispatching an action. Middleware extends the store's abilities, and lets you write async logic that interacts with the store.

Thunks are the recommended middleware for basic Redux side effects logic, including complex synchronous logic that needs access to the store, and simple async logic like AJAX requests.

- [redux-thunk](https://github.com/reduxjs/redux-thunk)

```javascript
$ npm install redux-thunk
```

#### Redux Logger
LogRocket is a production Redux logging tool that lets you replay problems as if they happened in your own browser. Instead of guessing why errors happen, or asking users for screenshots and log dumps, LogRocket lets you replay Redux actions + state, network requests, console logs, and see a video of what the user saw.

- [redux-logger](https://github.com/LogRocket/redux-logger)

```javascript
$ npm install redux-logger
$ npm install @types/redux-logger
```

#### Redux DevTools (Plugin)
- [Redux DevTools](https://github.com/reduxjs/redux-devtools)
- [Chrome Plugin](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd/related)
- [Firefox Plugin](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/)

### Hooks
- [Hooks API Reference](https://reactjs.org/docs/hooks-reference.html)
- [State Hook](https://reactjs.org/docs/hooks-state.html)
- [Rules of Hooks](https://reactjs.org/docs/hooks-rules.html)
- [Building Your Own Hooks](https://reactjs.org/docs/hooks-custom.html)

## Mockoon
Mockoon is the easiest and quickest way to run mock API servers locally. No remote deployment, no account required, free, open source and cross-platform.

- [Mockoon Website](https://mockoon.com/)
- [Mockoon Documentation](https://mockoon.com/docs/latest/about/)

## UI Frameworks

### Ant Design
[Website](https://ant.design/)
[Doc](https://ant.design/components/button/)

### Semantic UI React
[Website](https://semantic-ui.com/)
[Doc](https://react.semantic-ui.com/)

## TypeScript Cheatsheets
- [TypeScript Cheatsheets](https://github.com/typescript-cheatsheets/react-typescript-cheatsheet)

## Code Guidelines
- [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react)

## List of interesting packages
- [React Router](https://github.com/ReactTraining/react-router)

- [🏁 React Final Form](https://github.com/final-form/react-final-form#-react-final-form)

- [redux-form](https://github.com/redux-form/redux-form)

- [redux-saga](https://redux-saga.js.org/) > redux-saga is a library that aims to make application side effects (i.e. asynchronous things like data fetching and impure things like accessing the browser cache) easier to manage, more efficient to execute, easy to test, and better at handling failures.

- [Storybook](https://github.com/storybookjs/storybook) > Storybook is a development environment for UI components. It allows you to browse a component library, view the different states of each component, and interactively develop and test components.

- [Jest](https://github.com/facebook/jest) > 🃏 Delightful JavaScript Testing

- [Enzyme](https://github.com/enzymejs/enzyme) > Enzyme is a JavaScript Testing utility for React that makes it easier to test your React Components' output. You can also manipulate, traverse, and in some ways simulate runtime given the output. Enzyme's API is meant to be intuitive and flexible by mimicking jQuery's API for DOM manipulation and traversal.

- [react-virtualized](https://github.com/bvaughn/react-virtualized) > React components for efficiently rendering large lists and tabular data.

- [React DnD](https://github.com/react-dnd/react-dnd) > Drag and Drop for React.

- [React Intl](https://github.com/formatjs/react-intl) > Internationalize React apps. This library provides React components and an API to format dates, numbers, and strings, including pluralization and handling translations.