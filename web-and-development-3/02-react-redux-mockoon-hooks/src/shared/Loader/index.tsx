import React from 'react';
import './styles.scss';

const Loader = () => {
    
    return (
        <div className="loader col-12">
            <div className="loader-ring"></div>
        </div>
    );
}

export default Loader;