import React from 'react';
import { Link } from "react-router-dom";

import { CardProps } from './types';

import './style.scss';

const Card = (data: CardProps) => {

    const { tags, headline, date, description } = data;

    return(
        <div className="col-md-6">
            <div className="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                <div className="col p-4 d-flex flex-column position-static">
                    <strong className="d-inline-block mb-2 text-primary">{ tags }</strong>
                    <h3 className="mb-0">{ headline }</h3>
                    <div className="mb-1 text-muted">{ date }</div>
                    <p className="card-text mb-auto">{ description }</p>
                    <Link to="/main" className="stretched-link">Continue reading</Link>
                </div>
            </div>
        </div>
    );
};

export default Card;