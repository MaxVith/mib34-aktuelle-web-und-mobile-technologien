/**
 * Node Example
 */

// libs
const fs = require('fs');
const https = require('https');
const request = require('request');
const express = require('express');
const cheerio = require('cheerio');
const bodyParser = require("body-parser");
const morgan = require('morgan');
const cors = require('cors');

const app = express();

 // config
const { port, files } = JSON.parse(fs.readFileSync(__dirname + '/config.json'));

// set logger
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

// set header
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// routes
let browserVersions;

app.get('/versions/update', function (req, res) {
        
    request('https://updatemybrowser.org/', function (error, response, body) {
        
        if(!error){
            
            const date = new Date().toString();

            browserVersions = {
                "CHROME": cheerio.load(body)('.item.chrome .box > span').text(), 
                "SAFARI": cheerio.load(body)('.item.safari .box > span').text(),
                "EDGE": cheerio.load(body)('.item.edge .box > span').text(),
                "FIREFOX": cheerio.load(body)('.item.firefox .box > span').text(),
                "IE": cheerio.load(body)('.item.ie .box > span').text(),
                "OPERA": cheerio.load(body)('.item.opera .box > span').text()
            };

            fs.writeFile(files + 'versions.txt', date + '\n' + JSON.stringify(browserVersions), function(err) {
    
                if(err) {
                
                    return console.error(err);
                }
            }); 

            browserVersions = {
                "date": date.toString(),
                "versions": browserVersions
            };

            res.json(browserVersions);
        } else {
            
            res.json(error);
        }
    });
});

// get current list of browser versions
app.get('/versions', function (req, res) {
    
    if(!browserVersions){

        // example read file async
        fs.readFile(files + 'versions.txt', 'utf8', function (error, data) {
            
            let versions;

            try {

                versions = data.split('\n');
            } catch (e) {

                res.status(400).send('Invalid txt file');
            }

            browserVersions = {
                "date": versions[0],
                "versions": JSON.parse(versions[1])
            };

            res.json(browserVersions);
        });
    } else {

        res.json(browserVersions);
    }
});

// examples for post, put, delete requests
// app.post('/', function (req, res) {
//     res.send('Got a POST request');
// });
  
// app.put('/user', function (req, res) {
//     res.send('Got a PUT request at /user');
// });
  
// app.delete('/user', function (req, res) {
//     res.send('Got a DELETE request at /user');
// });

// example read file sync
const key = fs.readFileSync(__dirname + '/ssl/stern_web-arts_de.key');
const cert = fs.readFileSync(__dirname + '/ssl/stern_web-arts_de.crt');

// run the server now!
https.createServer({
    key: key,
    cert: cert,
}, app).listen(port, () => {

    console.log('>>> your server is running on port ' + port + ', https://localhost:' + port);
});