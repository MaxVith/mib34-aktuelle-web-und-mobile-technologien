const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mysql = require('mysql');

const app = express();

const mysqlConfig = {
    host: "example-mysql",
    port: 3306,
    user: "root",
    password: "secure",
    database: "example-db"
};

// set logger
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

// set header
app.use(cors());

// login route
app.get('/login', function (req, res) {

    console.log("> client request data", req.query);

    const username = req.query.username;
    const password = req.query.password;

    if(username === "user" && password === "password"){

        return res.status(200).json({
            status: "success",
            role: "user"
        });
    } else if(username === "admin" && password === "P@ssword!"){

        return res.status(200).json({
            status: "success",
            role: "admin"
        });
    }
    
    return res.status(404).json({
        status: "no way! invalid login"
    });
});

app.get('/user', function (req, res) {

    const account = req.query.account;

    if(!account) return res.status(404).json({
        error: "invalid user id"
    });

    try {

        const con = mysql.createConnection(mysqlConfig);

        con.connect(function(error) {

            if (error) {
                console.log(error);
                return res.status(404).json({
                    error: error
                });
            }
    
            console.log("> connected to example-mysql");
            console.log("> query on account", account);

            con.query('SELECT * FROM user WHERE account = "' + account + '"', function (error, result) {

                if(error) {
                    console.log(error);
                    return res.status(404).json({
                        error: error
                    });
                }

                return res.status(200).json({
                    payload: result
                });
            });
        });
    } catch(error) {
        
        console.log(error);
        return res.status(404).json({
            error: error
        });
    }
});

app.listen(8081, () => {
    console.log('>>> server is running on port 8081');
});