import { Dispatch } from 'redux';

import * as CONSTANTS from './constants';

/**
 * getCards
 */
export const getCards = () => 
    async (dispatch: Dispatch) => {
        
        dispatch({
            type: CONSTANTS.FETCH_REQUEST
        });
    };
