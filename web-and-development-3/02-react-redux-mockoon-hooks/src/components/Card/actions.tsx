import { Dispatch } from 'redux';

import * as CONSTANTS from './constants';

/**
 * getCards
 */
export const getCards = () => 
    async (dispatch: Dispatch) => {
        
        dispatch({
            type: CONSTANTS.FETCH_REQUEST
        });

        fetch('http://localhost:3001/cards')
            .then(response => response.json())
            .then(data => {

                dispatch({
                    type: CONSTANTS.FETCH_SUCCESS,
                    payload: data
                });
            }).catch(error => {

                dispatch({
                    type: CONSTANTS.FETCH_ERROR,
                    payload: error
                });
            });
    };