import { InitialState } from './types';

const initialState:InitialState = {
    card: {
        pending: true,
        payload: {}
    }
};

export default initialState;