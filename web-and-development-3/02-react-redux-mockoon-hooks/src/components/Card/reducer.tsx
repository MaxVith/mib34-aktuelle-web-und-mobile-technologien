import { ICardStateProps, CardActionTypes } from './types';
import * as CONSTANTS from './constants';

const initialCardState: ICardStateProps = {
    pending: true,
    payload: []
};

function cardReducer(state: ICardStateProps = initialCardState, action: CardActionTypes) {
    switch(action.type) {
        case CONSTANTS.FETCH_REQUEST:
            return {
                ...state, 
                pending: false
            }
        case CONSTANTS.FETCH_SUCCESS:
            return {
                ...state, 
                pending: false,
                payload: action.payload
            }
        case CONSTANTS.FETCH_UPDATE:
            console.log("do something");
            return {
                ...state
            }
        case CONSTANTS.FETCH_ERROR:
            return {
                ...state, 
                pending: false,
                error: action.error
            }
        default:
            return state;
    }
};

export default cardReducer;