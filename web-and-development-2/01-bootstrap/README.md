# Bootstrap
Die aktuelle Dokumentation von Bootstrap findet ihr immer unter https://getbootstrap.com/. Für das Customizen von Boostrap benötigt ihr SASS. In bestehenden Projekten wird dies i. d. R. durch bestehende Build-Systeme abgedeckt, z. B. Webpack. Hier verwenden wir als Beispiel direkt SASS. 

## SASS

### Installation unter Linux
Ihr benötigt Ruby, sollte bei Unix Systemen aber bereits installiert sein. Falls nicht...

$ sudo apt-get install ruby

### Sass installieren
Wenn Ruby auf dem Computer installiert ist, kann Sass installiert werden.

$ gem install sass 


### Installation überprüfen
Nach einer erfolgreichen Installation sollte dir die Versionsnummer ausgegeben werden. 

$ sass -v