import * as CONSTANTS from './constants';

interface ICardFetchRequestAction {
    type: typeof CONSTANTS.FETCH_REQUEST
}
  
interface ICardFetchSuccessAction {
    type: typeof CONSTANTS.FETCH_SUCCESS,
    payload: []
}
interface ICardFetchUpdateAction {
    type: typeof CONSTANTS.FETCH_UPDATE,
    payload?: []
}

interface ICardFetchErrorAction {
    type: typeof CONSTANTS.FETCH_ERROR,
    error: string
}

export type CardActionTypes = ICardFetchRequestAction | ICardFetchSuccessAction | ICardFetchUpdateAction | ICardFetchErrorAction;

export interface ICardStateProps {
    pending: boolean,
    payload?: [],
    errors?: string
}

export type CardProps = {
    tags: String,
    headline: String,
    date: String,
    description: String,
}