import { combineReducers } from 'redux';

import cardReducer from '../components/Card/reducer';

const rootReducer = combineReducers({
    card: cardReducer
});

export type RootReducerState = ReturnType<typeof rootReducer>;

export default rootReducer;
