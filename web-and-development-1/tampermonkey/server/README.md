# Tampermonkey Server

## How to use

### Node
Im ersten Schritt benötigt ihr Node. Node findet ihr unter https://nodejs.org/en/, https://nodejs.org/en/download/ sowie eine Dokumentation für die Installation. 

### Server
Euren Server startet ihr wie folgt: 

$ npm install
$ node server_tampermonkey.js

### Dateien ausliefern 
Nachdem euer Server gestartet wurde, könnt ihr über https://localhost:9001/ eure Dateien erreichen, z. B. https://localhost:9001/very-important-file.js. 