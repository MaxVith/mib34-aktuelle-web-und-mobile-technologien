import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import TeaserContainer from './components/Teaser';
import CardContainer from './components/Card';
import Main from './components/Main';

import './App.scss';

function App() {
  return (
    <div className="App">
      
      <div id="root" className="container">
        <Router>
          <Switch>
            <Route path="/main" component={ Main } />
            <Route path="/">
              <TeaserContainer />
              <CardContainer />
            </Route>
          </Switch>
        </Router>
      </div>
      
      <footer className="container">
        <div className="row">
          <div className="col-12">
            <hr />
            <p>Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default App;
