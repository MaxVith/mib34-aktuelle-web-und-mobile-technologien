import { ICardStateProps } from '../components/Card/types';

export interface InitialState {
    card: ICardStateProps
}