/**
 * Interfaces 
 * http://www.typescriptlang.org/play/
 */
interface User {
    firstName: string;
    lastName: string;
    city?: string;
}

const user: User = {
    firstName: 'John',
    lastName: 'Doe',
    city: 'London'
};

function print(user: User) {
    console.log(user.firstName, user.lastName);
}

print(user);