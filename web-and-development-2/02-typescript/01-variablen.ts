/**
 * Variablen 
 * http://www.typescriptlang.org/play/
 */
// Boolean
let aBoolean: boolean = false;
aBoolean = true;

// Number
let aNumber: number = 0;
aNumber = 1.2;

// String
let aString: string = "A String";
aString = 'A String';
aString = `A String`;

// Array
const array1: number[] = [1, 2, 3];
// generische Typdefinition
const array2: Array<number> = [1, 2, 3];


// Enum
// Wird für Aufzählungen verwendet. Die Werte von Enums sind Zahlen. Seit v2.4 können 
// statt Zahlen auch Strings verwendet werden
enum Status {Done, InProgress, New};
const a: Status = Status.Done;

// v2.4+
enum StatusString {Done = 'DONE', Inprogress = 'PROG', New = 'NEW'};
const b: StatusString = StatusString.Done;

// Any
// Kein echter Typ
const unknown: any = getData();