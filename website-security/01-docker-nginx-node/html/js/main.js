
(function(){
    "use static";

    function ready(fn) {
        if (document.readyState != 'loading'){
            fn();
        } else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    }

    ready(() => {

        document.querySelector('#loginForm').addEventListener("submit", (event) => {
            
            event.preventDefault();

            fetch('//' + location.hostname + ':8081/login?username=' + document.querySelector('#inputEmail').value + '&password=' + document.querySelector('#inputPassword').value).then((response) => {

                    console.log("> server response", response);
                }).catch(error => {
                    
                    console.error(error);
                });

            return false;
        });
    });
})();