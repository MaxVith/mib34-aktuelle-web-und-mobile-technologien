/**
 * Funktionen 
 * http://www.typescriptlang.org/play/
 */
// Benannte Funktion
function foo(a: number): number {
    return a;
}

function foo(a: number, b: string): void {}

// zu wenig Parameter
foo(1);

// zu viele Parameter
foo(1, '2', 3);

// falsche Reihenfolge
foo('2', 1);