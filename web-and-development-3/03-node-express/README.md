# Node Server

## How to run

```javascript
$ npm install
```

Der Server ist erreichbar unter: http://localhost:9001

## Express
- [Express](https://expressjs.com/de/)

## Prozessmanager

- [StrongLoop Process Manager](http://strong-pm.io/)
- [pm2](https://github.com/Unitech/pm2)
- [forever](https://www.npmjs.com/package/forever)

### pm2
PM2 is a production process manager for Node.js applications with a built-in load balancer. It allows you to keep applications alive forever, to reload them without downtime and to facilitate common system admin tasks.

Modul um den Server als Task im Hintergrund laufen zu lassen:  
https://www.npmjs.com/package/pm2

pm2 bei einer lokalen Installation aufrufen (falls nicht global installiert):  
- Server status: ./node_modules/pm2/bin/pm2 list  
- Server starten: ./node_modules/pm2/bin/pm2 start node_server.js  
- Server stoppen: ./node_modules/pm2/bin/pm2 stopp all  