import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import store from './store/';

import TeaserContainer from './components/Teaser';
import CardContainer from './components/Card';
import Main from './components/Main';

import './App.scss';

function App() {
    return (
        <div className="App">
            <div id="root" className="container">
                <Provider store={store}>
                    <Router>
                        <Switch>
                            <Route path="/main" component={ Main } />
                            <Route path="/">
                                <TeaserContainer />
                                <CardContainer />
                            </Route>
                        </Switch>
                    </Router>
                </Provider>
            </div>
            <footer className="container">
                <div className="row">
                    <div className="col-12">
                        <hr />
                        <p>Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default App;
